<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240519133347 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"');

        $this->addSql('CREATE TABLE users 
        (
    guid UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4() , 
    name VARCHAR not null, 
    email VARCHAR not null , 
    phone VARCHAR, 
    verify BOOLEAN DEFAULT false, 
    password VARCHAR not null, 
    created_at TIMESTAMP not null default current_timestamp, 
    updated_at TIMESTAMP not null default current_timestamp)'
        );

    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE users');
    }
}
