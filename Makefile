#CONTAINER START
start:
	docker-compose up -d
#open WORKDIR in php container
php_open:
	docker-compose exec php bash
docker_restart:
	docker-compose down
	docker-compose up -d