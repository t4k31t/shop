<?php

namespace App\Entity;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;


#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Task
{
    #[ID]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Column(type: 'integer')]
    private int $id;
    #[ManyToMany(targetEntity: Users::class, inversedBy: "Task")]
    #[JoinTable(name: "user_tasks")]
    #[JoinColumn(name: "guid", referencedColumnName: "id")]
    #[InverseJoinColumn(name: "user_id", referencedColumnName: "guid", nullable:'false')]
    private Collection $user;
    #[Column(type: 'string', nullable: 'false')]
    private string $title;
    #[Column(type: 'string', nullable: 'false')]
    private string $description;
    #[Column(type: 'string', nullable: 'false')]
    private string $status;
    private \DateTime $createdAt;
    private \DateTime $updatedAt;
}