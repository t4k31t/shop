<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Doctrine\Common\Collections\Collection;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Users
{
    #[Id]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[ORM\Column(type: 'guid', unique: 'true', nullable: 'false')]
    private string $guid;

    //ManyToMany
    #[ManyToMany(targetEntity: Task::class , mappedBy: "users")]
    private Collection $tasks;
    #[ORM\Column(type: 'string', nullable: 'false')]
    private string $name;
    #[ORM\Column(type: 'string', unique: 'true', nullable: 'false')]
    private string $email;
    #[ORM\Column(type: 'string', unique: 'true')]
    private string $phone;
    #[ORM\Column(type: 'string', unique: 'true', nullable: 'false')]
    private bool $verify = false;
    #[ORM\Column(type: 'string', unique: 'true', nullable: 'false')]
    private string $password;
    #[Column(type: 'datetime')]
    private ?\DateTime $createdAt = null;
    #[Column(type: 'datetime', nullable: false)]
    private \DateTime $updatedAt;

    public function getGuid(): string
    {
        return $this->guid;
    }
    public function setGuid(string $guid): self
    {
        $this->guid = $guid;
        return $this;
    }

    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function setTasks(Collection $tasks): self
    {
        $this->tasks = $tasks;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    public function getVerify(): bool
    {
        return $this->verify;
    }

    public function setVerify(bool $verify): self
    {
        $this->verify = $verify;
        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
    #[ORM\PreFlush]
    public function preFlush(): void
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }

}
